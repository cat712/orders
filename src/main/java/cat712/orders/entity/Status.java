package cat712.orders.entity;

/**
 * Перечисление для описания статуса заказа
 * @version 1.0
 */
public enum Status {
    CREATE, PAID, COLLECTION, READY;
}
