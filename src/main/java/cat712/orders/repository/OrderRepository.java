package cat712.orders.repository;

import cat712.orders.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Класс-репозиторий для связи с таблицой объектов типа Order
 * @version 1.0
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
}
