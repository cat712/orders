package cat712.orders.repository;

import cat712.orders.entity.Unit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Класс-репозиторий для связи с таблицой объектов типа Unit
 * @version 1.0
 */
@Repository
public interface UnitRepository extends JpaRepository<Unit, Long> {
    // поиск единицы измерения по сокращённому наименованию
    Optional<Unit> findByShortName(String shortName);
    // удаление единицы измерения по сокращённому наименованию
    void deleteByShortName(String shortName);
}
