package cat712.orders.repository;

import cat712.orders.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Класс-репозиторий для связи с таблицой объектов типа Product
 * @version 1.0
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    // поиск товара по наименованию
    Optional<Product> findByName(String name);
    // поиск товаров по содержанию наименования
    List<Product> findByNameContainingIgnoreCase(String name);
    // поиск товара по изображению
    Optional<Product> findByImage(String image);
}
